import serial
import pynmea2
import datetime

def parse_gps(line):
    msg = pynmea2.parse(line)
    date = datetime.datetime.now()
    print "datetime : %s, timestamp : %s, coord : %s, %s,  alti : %s %s" % (( date, msg.timestamp, msg.latitude, msg.longitude, msg.altitude,msg.altitude_units))


serial_port = serial.Serial('/dev/serial0', 9600, timeout=5)
while True:
    line = serial_port.readline().decode('unicode_escape')

    if 'GGA' in line:
        parse_gps(line)
